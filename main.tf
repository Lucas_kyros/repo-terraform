

data "aws_iam_policy_document" "policy" {
  statement {
    sid    = ""
    effect = "Allow"

    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  name               = "iam_for_lambda"
  assume_role_policy = data.aws_iam_policy_document.policy.json
}

resource "aws_lambda_function" "lambda" {
  function_name = "lambda_node"

  filename      = "backend.zip"

  role    = aws_iam_role.iam_for_lambda.arn
  handler = "index.test"
  runtime = "nodejs12.x"

  environment {
    variables = {
      greeting = "Welcomee"
    }
  }
}

#resource "aws_s3_bucket" "meu-bucket" {
#  bucket = "lukazprs-gitlab01"
#
#  tags = {
#    Name        = "terraform-cloud gitlab"
#    Environment = "Test gitlab"
#  }
#}
#
#resource "aws_s3_bucket_acl" "minha-acl" {
#  bucket = aws_s3_bucket.meu-bucket.id
#  acl    = "private"
#}
